\documentclass[aps, pra, nofootinbib, onecolumn, 11pt, tightenlines, 
notitlepage, superscriptaddress, longbibliography]{revtex4-1}

\include{macros}

\begin{document}

\title{Haar B}
\author{Joel J. Wallman}
\affiliation{Institute for Quantum Computing and Department of Applied 
Mathematics, University of Waterloo, Waterloo, Ontario N2L 3G1, Canada}
\date{\today}

\begin{abstract}
\end{abstract}

\pacs{03.65.Aa, 03.65.Wj, 03.65.Yz, 03.67.Lx, 03.67.Pp}

\maketitle

\section{Preliminaries}

Throughout this paper, we denote by $\tilde{\mc{C}}$ a noisy implementation of a 
quantum channel $\mc{C}$. A unitary channel $\mc{U}$ corresponds to conjugation 
by a unitary, that is, $\mc{U}(\rho) = U\rho U\ct$. We denote averages by 
\begin{align}
\la g(X) \ra_X = \sum_{X\in\mc{X}} p(X) g(X)
\end{align}
where the set $\mc{X}$ and the probability distribution are implicit (the 
probability distribution is typically the uniform Haar measure).

A Markovian implementation of a unitary channel $\mc{G}$ is a linear map 
$\tilde{\mc{G}}:\mbb{C}^{d\times d}\to\mbb{C}^{d\times d}$, henceforth referred
to as a channel. It is common to restrict attention to completely positive and 
trace-preserving (CPTP) channels, that is, channels that map all valid quantum 
states to valid quantum states, but this restriction does not change the general 
behavior of randomized benchmarking. The error in an implementation 
$\tilde{\mc{G}}$ of $\mc{G}$ can be quantified by the average gate fidelity of 
$\tilde{\mc{G}}$ to $\mc{G}$,
\begin{align}
f(\tilde{\mc{G}},\mc{G}) 
= \int d\psi \tr[\mc{G}(\psi) \tilde{\mc{G}}(\psi)],
\end{align}
where $d\psi$ is the unitarily invariant Haar measure on the space of pure 
states. For convenience, we define 
\begin{align}
f(\mc{L}) = f(\mc{L},\mc{I}) = \int d\psi \tr[\psi \mc{L}(\psi)]
\end{align}
to be the average gate fidelity of $\mc{L}$ to the identity (hereafter simply 
the fidelity).

We can represent states and measurements by dual vectors and channels by 
matrices as follows. Let $\{e_1,\ldots,e_d\}$ be the canonical orthonormal unit 
basis of $\mbb{C}^d$ and $\{B_1,\ldots,B_{d^2}\}$ be a trace-orthonormal basis 
of $\mbb{C}^{d\times d}$, that is, $\tr(B_j\ct B_k) = \delta_{j,k}$. Defining 
the linear map $|*\raa:\mbb{C}^{d\times d}\to\mbb{C}^{d^2}$ by
\begin{align}
|A\raa = \sum_j \tr(B_j\ct A)e_j
\end{align}
and the adjoint map $\laa A| = |A\raa\ct$, we have $\laa A|B\raa = \tr(A\ct B)$.
A channel $\mc{C}$ maps $\rho$ to $\mc{C}(\rho)$, which can be represented
by the matrix
\begin{align}\label{eq:matrix_rep}
\mc{C} = \sum_j |\mc{C}(B_j)\raa\!\laa B_j|,
\end{align}
where we abuse notation slightly by using the same notation for the abstract
channel and its matrix representation. For the remainder of the paper, we 
assume 
the operator basis is Hermitian and that $B_1 = \ds{I}_d/\sqrt{d}$, so that the 
matrix representation of any Hermiticity-preserving map (including all CPTP 
maps) is real-valued.

The matrix representation (also known as the Liouville and natural 
representations) is convenient for long sequences because channel composition 
is represented by matrix multiplication. We denote the non-commutative product 
by
\begin{align}
x_{a:b} = \prod_{j=a}^b x_j  = \begin{cases}
x_a x_{a-1} \ldots x_b & a\geq b \\
1 & a<b.
\end{cases}
\end{align}

We will also use the vectorization map
\begin{align}
\opv(\sum v_j e_j\ct) = e_j\otimes v_j,
\end{align}
which maps a matrix to a vector by stacking the columns vertically. The 
vectorization map satisfies the identity
\begin{align}\label{eq:vec_product}
\opv (ABC) = (C^T\otimes A)\opv (B).
\end{align}

The following property of unitary 2-designs is the key conceptual tool used to 
derive the RB decay curve~\cite{Nielsen2002,Dankert2009}, so that averaging over 
random sequences `twirls' the noise at each time step into a diagonal channel.
Any pair $(\omega,\mbb{G})$ satisfying the following property is a weighted 
unitary 2-design~\cite{Gross2007a}, where $\omega$ is a probability 
distribution over $\mbb{G}$. The canonical example of a unitary 2-design is the 
uniform distribution over the $n$-qubit Clifford group.

\begin{lem}\label{lem:twirl}
For any untary 2-design $\mbb{G}$ and channel $\mc{C}$,
\begin{align}
\ds{E}_G (\mc{G}\ct \mc{CG}) = \mc{T_C} = \mc{C}_{11}|B_1\raa\!\laa B_1| 
+ p(\mc{C})[\ds{I}_{d^2} - |B_1\raa\!\laa B_1|],
\end{align}
where $B_1 = \ds{I}_d/\sqrt{d}$ and
\begin{align}\label{eq:decay_parameter}
p(\mc{C}) &= \frac{\tr(\mc{C})-\mc{C}_{11}}{d^2-1} = 
\frac{df(\mc{C})-\mc{C}_{11}}{d-1}.
\end{align}
\end{lem}

\begin{proof}
The homomorphism $U\to \mc{U}$ has two inequivalent subrepresentations of 
$\mr{U}(d)$~\cite{Gross2007a}. For any unitary $U\in \mr{U}(d)$, $\mc{U}(B_1) = 
U B_1 U\ct = B_1$ and so $|B_1\raa$ carries a trivial representation of 
$\mr{U}(d)$ and any subgroup thereof and the complementary space carries an 
inequivalent irreducible representation of $\mr{U}(d)$. As 
$\int_{\mr{U}(d)} dU\, \mc{U}\ct \mc{CU}$ commutes with the action of 
$\mr{U}(d)$ where $dU$ is the Haar measure over $\mr{U}(d)$,
\begin{align}\label{eq:twirled_map}
\int_{\mr{U}(d)} dU\, \mc{U}\ct \mc{CU} = a|B_1\raa\!\laa B_1| + 
b(\ds{I}_{d^2} - |B_1\raa\!\laa B_1|)
\end{align}
for some scalars $a$ and $b$ by Schur's Lemma. As $|B_1\raa$ carries a trivial 
representation of $\mr{U}(d)$, that is, $\mc{U} = |B_1\raa\!\laa B_1 + M$ for 
some $M\in\mbb{C}^{d^2-1\times d^2-1}$ with $M|B_1\raa = \laa B_1|M = 0$, 
$a = \mc{L}_{11}$. As the trace is a linear map and is invariant under cyclic 
permutations of the arguments,
\begin{align}
\tr\Bigl(\int_{\mr{U}(d)} dU\, \mc{U}\ct \mc{CU}\Bigr)
&= \int_{\mr{U}(d)} dU\,  \tr(\mc{U}\ct \mc{CU}) \notag\\
&= \int_{\mr{U}(d)} dU\,  \tr(\mc{C}) \notag\\
&= \tr(\mc{C}).
\end{align}
Therefore taking the trace of both sides of \cref{eq:twirled_map} gives 
\begin{align}
b = \frac{\tr(\mc{C})-\mc{C}_{11}}{d^2-1}.
\end{align}
For the final equality, note that
\begin{align}
f(\mc{C}) &= \int d\psi \tr[\psi \mc{C}(\psi)] \notag\\
&= \int d\psi\,dU \tr[\psi \mc{U}\ct\mc{CU}(\psi)] \notag\\
&= \int d\psi \tr[\psi \mc{T_C}(\psi)] \notag\\
&= \int d\psi \tr[\psi \mc{T_C}(\psi-\tfrac{1}{d}\ds{I}_d)]
+\int d\psi \tr[\psi \mc{T_C}(\tfrac{1}{d}\ds{I}_d)] \notag\\
&= \frac{(d-1)p(\mc{C}) + \mc{C}_{11}}{d}.
\end{align}
The above analysis holds for averaging over the full unitary group. Now note
that the matrix entries of $\ds{E}_U (\mc{U}\ct \mc{CU})$ are
\begin{align}
\laa B_j |\ds{E}_U (\mc{U}\ct \mc{CU})|B_k\raa
&= \ds{E}_U \tr\Bigl(B_j\ct \mc{U}\ct \mc{CU}[B_k]\Bigr) \notag\\
&= \ds{E}_U \tr\Bigl(B_j\ct U\ct\mc{C}[U B_k U\ct] U\Bigr),
\end{align}
which, in particular, are polynomials of degree two in the entries of $U$ and 
$\bar{U}$. Therefore 
\begin{align}
\ds{E}_G (\mc{G}\ct \mc{CG})
= \ds{E}_U (\mc{U}\ct \mc{CU})
= \mc{T_C} 
\end{align} 
for any weighted 2-design $(\omega,\mbb{G})$~\cite{Dankert2009}.
\end{proof}

\section{Randomized benchmarking protocol}

\begin{figure}
\Qcircuit @C=.7em @R=.3em @!R {
\lmeasure{Q} & \gate{G_{m+1}} & \gate{G_m} & \qw & \ldots & & \gate{G_2} & 
\gate{G_1} & \measure{\psi} \\
\lmeasure{Q} & \gate{\tilde{\mc{G}}_{m+1}} & \gate{\tilde{\mc{G}}_m} & \qw & 
\ldots & & \gate{\tilde{\mc{G}}_2} & \gate{\tilde{\mc{G}}_1} & \measure{\rho} 
\\}
\caption{a) Ideal randomized benchmarking circuit from right to left, where the 
gates $G_j$ are chosen uniformly at random from a group $\mbb{G}$ that is also 
a 
unitary 2-design and $G_{m+1}^* = G_m\ldots G_1$. b) Corresponding experimental 
circuit, where $\tilde{\mc{G}}$ is the experimental implementation of the 
channel $\mc{G}(\rho) = G\rho G\ct$ with Markovian noise.}
\label{fig:rb_circuit}
\end{figure}

The following randomized benchmarking protocol is a powerful tool for 
efficiently characterizing the quality of the experimental implementation of a 
group of operations $\mbb{G}$ that is also a unitary 
2-design~\cite{Dankert2009}. 
For brevity, we denote averaging by 
$\ds{E}_{x\in X} f(x) = |X|^{-1}\sum_{x\in X}f(x)$, with the set typically left 
implicit.

\begin{enumerate}
	\item Choose a positive integer $m$.
	\item Uniformly and randomly choose a sequence of $m$ elements of $\mbb{G}$,
	denoted $\vec{G} = (G_1,\ldots,G_m)\in\mbb{G}^m$ and set $G_{m+1}\ct = 
	G_{m:1}$.
	\item Estimate the expectation value (also known as the survival 
	probability) $Q_{\vec{G}}$ of an observable $Q$ after preparing a $d$-level 
	system in a state $\rho$ and applying the sequence of $m+1$ operations 	
	$G_1,
	G_2,\ldots,G_{m+1}$ as shown in \cref{fig:rb_circuit}.
	\item Repeat steps 2--3 to estimate the average over random sequences 
	$\ds{E}_{\vec{G}}Q_{\vec{G}}$ to a desired precision.
	\item Repeat steps 1--4 for different values of $m$ and fit to the decay 
	model
	\begin{align}\label{eq:fidelity_decay}
	\ds{E}_{\vec{G}}Q_{\vec{G}} = Ap^m + B
	\end{align}
	to estimate $p$~\cite{Magesan2011}.
\end{enumerate}

For gate- and time-independent Markovian noise, that is, for noise such that 
$\tilde{\mc{G}} = \mc{EG}$ for some constant completely positive and 
trace-preserving (CPTP) map $\mc{E}$, the decay parameter $p$ is related to the 
fidelity of $\mc{E}$ by \cref{eq:decay_parameter} and the constants $A$ and $B$ 
in \cref{eq:fidelity_decay} incorporate SPAM errors~\cite{Magesan2011}.

\section{Irreducible representations of the unitary group}

We now identify subrepresentations of $\mc{U}\tn{2}$.

Important: for $t\geq d$,
\begin{align}\label{eq:frame_potential}
\int dU \lv \tr U\rv^{2t} = t!
\end{align}
(Roy and Scott)

\begin{lem}
The following subspaces of $\mbb{C}^{d^4}$ are invariant under the action of
$\mc{U}\tn{2}$.
\begin{align}
V_{t}^1 &= \frac{1}{d}|\ds{I}_{d^2}\raa \notag\\
V_{t}^P &= \sum_{P\in\bs{\sigma}} |P\otimes P\raa \notag\\
V_O^L &= \{|P\otimes \ds{I}_{d^2}\raa:P\in\bs{\sigma} \} \notag\\
V_O^R &= \{|\ds{I}_{d^2}\otimes P\raa:P\in\bs{\sigma} \} \notag\\
V_{[+]} &=\tfrac{1}{\sqrt{2}}\{|P \otimes Q\raa+|Q\otimes P\raa:[P,Q]=0,P,Q\in\bs{\sigma} \} \notag\\
V_{\{+\}} &=\tfrac{1}{\sqrt{2}}\{|P \otimes Q\raa+|Q\otimes P\raa:\{P,Q\}=0,P,Q\in\bs{\sigma} \} \notag\\
V_{-} &=\tfrac{1}{\sqrt{2}}\{|P \otimes Q\raa-|Q\otimes P\raa:P,Q\in\bs{\sigma} \}
\end{align}
\end{lem}

\begin{proof}
All the spaces are invariant.

By Schur's orthogonality relations and \cref{eq:frame_potential},
\begin{align}
\sum_R n_R^2 &= \int dU \lv\chi_L(U)^2\rv^2 \notag\\
&= \int dU \lv \tr(U)\rv^8 \notag\\
&= 24.
\end{align}
Furthermore, as $\mbb{C}^{d^2}\equiv V_t \oplus V_O$,
\begin{align}
\sum_{R=O,t} n_R &= \int dU \chi_{t\oplus O}(U)\chi_L(U)^2 \notag\\
&= \int dU \lv \tr(U)\rv^6 \notag\\
&= 6
\end{align}
and so $n_t = 2$ and $n_O = 4$.

For the antisymmetric subspace,
\begin{align}
\chi_{-}(U) &= \sum_{P,Q\in\bs{\sigma}} \frac{1}{2}\laa P\otimes Q - Q\otimes P|\mc{U}\tn{2}|P\otimes Q - Q\otimes P\raa\notag\\
&= \sum_{P,Q\in\bs{\sigma}} \laa P\otimes Q|\mc{U}\tn{2}|P\otimes Q\raa  - \laa P\otimes Q|\mc{U}\tn{2}|Q\otimes P\raa \notag\\
&= (\sum_{P\in\bs{\sigma}} \laa P|\mc{U}|P \raa)^2 - \sum_{P,Q\in\bs{\sigma}} \laa P|\mc{U}|Q\raa\!\laa Q|\mc{U}|P\raa \notag\\
&= (\sum_{P\in\bs{\sigma}} \laa P|\mc{U}|P \raa)^2 - \sum_{P\in\bs{\sigma}} \laa P|\mc{U}^2|P\raa
\end{align}

Missing factor of 2:
\begin{align}
\chi_{\pm}(U) = \tfrac{1}{2}[\chi_V(U)\pm \chi_V(U^2)]
\end{align}

\end{proof}


\section{Remark on lower bound}

The paper with Jonas shows that the worst-case variance is small. Let's say we have a bound that optimizes over all the SPAM and says that $\mathbb{V} \le \max_{Q,\rho} \mathbb{V}(Q,\rho) := \mathbb{V}_{\max}$. I'm not sure we discuss this in that paper, but let's assume we have such a bound. Suppose that our lower bound gives us a bound of the form $\mathbb{V} \ge c \mathbb{V}_{\max}$ for some $c > 0$.  Now consider all SPAM errors that are $\epsilon$ away from the ideal case in some suitable norm (which shouldn't be hard to figure out).  Then we should get a SPAM-free lower bound of the form 
\begin{align}	\mathbb{V} \ge c \mathbb{V}_{\max} - (2 \epsilon - \epsilon^2) \mathbb{V}_{\max} \,. \end{align}
As long as $c > 2 \epsilon -\epsilon^2$ this is a nontrivial bound and the variance would then be tight up to constant factors. 

\bibliography{library}

\end{document}